# Set subdomain for PROD, TEST and DEV webapp links on a reporting server

Allow the user to specify the domain for links on a page

## Background

You have a internal webserver built with Drupal 7 that supports reporting and system administration for a web application. Data from the web application is displayed using [Views Database Connector](https://www.drupal.org/project/views_database_connector). Also, basic system admin tasks are included in the reports using URLs for the web application. For instance, a personnel report might show personnel data as well as links to the personnel records in the web application.

Your web application has a PRODUCTION, TEST and DEV instance each with their own subdomain (e.g., myserver.example.com, myserver-test.example.com, myserver-dev.example.com).

You want your report listings to be able to point to either PROD, TEST or DEV, on demand. You could specify the correct subdomain in the data when it is pulled from the web application. This would make sense. But even still, you might want to switch subdomains on a whim. The links would still likely be legitimate as TEST and DEV are usually copies of PROD.

### Possible approaches:

Create a server choice field in each user's profile so the user may select which server the web application links should point to. The VDC module doesn't permit any way to get user profile fields into the view. The custom text view field does not support tokens, otherwise, [Custom Tokens](https://www.drupal.org/project/token_custom) might be a solution.

Use Views PHP and the ID of the logged in user from the view's Contextual Filters. This might be possible, but the solution would not be very maintainable or future proof, and Views PHP is discouraged and intentionally difficult to use.

Several other avenues could be pursued using Views Conditional Fields, custom parameters on the URL.

In the end, the simplest approach is to use javascript on the page to alter the web application links based on the value of a select list.

## Example

We will tag each CSS class related to our web application a common prefix, say `app-`. Then, each web application link in the view will have a class of `app-serveruri`. 

Add a global text area to the views header and set it to allow full HTML. Set it's value to

```html
<!-- some_report_vw -->
<p>This is the header text explaining what this view listing is for.</p>

<p>For
<select class="app-server width-auto"><option value="myserver">PROD</option>
<option value="myserver-test">TEST</option>
<option value="myserver-dev">DEV</option></select>
</p>

<script type="text/javascript">
	jQuery(function() {

		let reServer = /(.*?)(myserver-test|myserver-dev|myserver)(\.example\.com.*)/i; 
		
		jQuery('.app-server').change(function () {
			_changeServerUrl(jQuery(this));
		});
		
		function _changeServerUrl(o) {
			var server = o.val() || 'myserver';
			jQuery('.app-serveruri').each(function(i, e) {
				var a = jQuery(this);
				var url = a.attr('href');
				var newUrl = url.replace(reServer, '$1'+server+'$3');
				//console.log(newUrl);
				a.attr('href', newUrl);
			});
		}
	});
</script>
<!-- some_report_vw -->
```

Use [CSS Injector](https://www.drupal.org/project/css_injector) to add custom CSS

```css
.width-auto {
    width: auto;
}
```
## How this works

Most of the work is done by the regular expression string replace:

```javascript
let reServer = /(.*?)(myserver-test|myserver-dev|myserver)(\.example\.com.*)/i;
var newUrl = url.replace(reServer, '$1'+server+'$3');
```

This regular expression finds the subdomain of the url and replaces it with the selected value. It changes myserver.example.com to myserver-test.example.com and conveniently allows you to change back: myserver-test.example.com back to myserver.example.com.

See [regex101](https://regex101.com/) for testing your regular expressions.

## Benefits

Using this simple approach to swap the link's href allows you to easily hover over the link and verify that the subdomain is changing correctly. 

If you determine the target subdomain using a link click handler, you would have difficulty verifying links that delete content without actually clicking them and possibly deleting the content.


